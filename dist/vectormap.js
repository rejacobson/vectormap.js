(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.VectorMap = factory();
    }
}(this, function () {

  var VectorMap = function(points) {
    this.points     = points;
    this.rangemap   = {};
    this.resolution = 10;
    this.rect     = {top:0, bottom:0, left:0, right:0};

    this._update();
  };

  VectorMap.prototype = {
    forEachRange: function(start, end, callback) {
      start = Math.floor(start/this.resolution) * this.resolution;
      end   = Math.ceil(end/this.resolution) * this.resolution;

      for (var i=this.rangemap[start][0]; i<this.rangemap[end][1]; i++) {
        if (callback) callback(this.points[i], this.points[i+1]);
      }
    },

    inBounds: function(p) {
      return !(p[0] < this.rect.left ||
               p[0] > this.rect.right ||
               p[1] < this.rect.top ||
               p[1] > this.rect.bottom);
    },

    breakup: function(min_size) {
      this.points = breakupPoints(this.points, min_size);
      this._update();
    },

    simplify: function() {
      this.points = simplifyPoints(this.points);
      this._update();
    },

    smoothen: function(roughness) {
      roughness = roughness || 15;
      this.points = smoothenPoints(this.points, roughness);
      this._update();
    },

    _update: function() {
      this._create_range_map();
      this._find_boundaries();
    },

    _find_boundaries: function() {
      this.rect = _find_boundaries(this.points);
    },

    _create_range_map: function() {
      this.rangemap = _create_range_map(this.points, this.resolution);
    }
  };

  function _create_range_map(points, resolution) {
    var rangemap = {};
    var x, y;
    var last_x;

    for (var i=0, len=points.length; i<len; i++) {
      x = Math.floor(points[i][0]/resolution) * resolution;

      if (!rangemap[x]) rangemap[x] = [i, i];
      if (rangemap[x][1] < i) rangemap[x][1] = i;

      if (last_x !== null) {
        if (Math.abs(x - last_x) > resolution) {
          var dir = (last_x > x ? -1 : 1);
          var inc = resolution * dir;
          for (var j=last_x; 0<((x - j) * dir); j+=inc) {
            if (!rangemap[j]) rangemap[j] = [i, i];
            if (rangemap[j][1] < i) rangemap[j][1] = i;
          }
        }
      }

      last_x = x;
    }

    return rangemap;
  };

  function _find_boundaries(points) {
    rect = {left:null, right:null, top:null, bottom:null};

    for (var i=0, len=points.length; i<len; i++) {
      if (rect.left   == null || rect.left   > points[i][0]) rect.left   = points[i][0];
      if (rect.right  == null || rect.right  < points[i][0]) rect.right  = points[i][0];
      if (rect.top    == null || rect.top    > points[i][1]) rect.top    = points[i][1];
      if (rect.bottom == null || rect.bottom < points[i][1]) rect.bottom = points[i][1];
    }

    return rect;
  };

  function breakupPoints(points, min_length) {
    var new_points = [];
    min_length = min_length * min_length;

    function _length_sq(p1, p2) {
      return Math.pow(p2[0] - p1[0], 2) + Math.pow(p2[1] - p1[1], 2);
    }

    function _reduce(p1, p2) {
      if (_length_sq(p1, p2) <= min_length) return p1;
      var pts = [];
      var p3 = [p1[0] + ((p2[0] - p1[0]) * 0.5), p1[1] + ((p2[1] - p1[1]) * 0.5)];
      pts = pts.concat(_reduce(p1, p3));
      pts = pts.concat(_reduce(p3, p2));
      return pts;
    }

    for (var i=0, len=points.length-1; i<len; i++) {
      if (_length_sq(points[i], points[i+1]) > min_length) {
        new_points = new_points.concat(_reduce(points[i], points[i+1]));
      }
    }

    new_points.push(points[points.length-1]);

    return new_points;
  };

  function simplifyPoints(points) {
    var new_points = [];

    for (var i=0, len=points.length; i<len; i+=2) {
      new_points.push(points[i]);
    }

    if (new_points.length % 2 == 0) new_points.push(points[points.length-1]);

    return new_points;
  };

  function smoothenPoints(points, roughness) {
    if (points.length < 4) return;

    var new_points  = [];
    var pts         = plotBezierQuad(points[0], points[0], points[1], points[2], roughness);
    new_points = new_points.concat(pts);

    for (var i=0, len=points.length-4; i<len; i++) {
      new_points = new_points.concat(plotBezierQuad(points[i], points[i+1], points[i+2], points[i+3], roughness));
    }

    new_points = new_points.concat(plotBezierQuad(points[i+1], points[i+2], points[i+3], points[i+3], roughness));
    new_points = new_points.concat(plotBezierQuad(points[i+2], points[i+3], points[i+3], points[i+3], roughness));

    return new_points;
  };

  function findControlPoints(s1, s2, s3) {
     var dx1 = s1[0] - s2[0], dy1 = s1[1] - s2[1],
         dx2 = s2[0] - s3[0], dy2 = s2[1] - s3[1],

         l1 = Math.sqrt(dx1*dx1 + dy1*dy1),
         l2 = Math.sqrt(dx2*dx2 + dy2*dy2),

         m1 = [(s1[0] + s2[0]) / 2.0, (s1[1] + s2[1]) / 2.0],
         m2 = [(s2[0] + s3[0]) / 2.0, (s2[1] + s3[1]) / 2.0],

         dxm = (m1[0] - m2[0]),
         dym = (m1[1] - m2[1]),

         k = l2 / (l1 + l2),
         cm = [m2[0] + dxm*k, m2[1] + dym*k],
         tx = s2[0] - cm[0],
         ty = s2[1] - cm[1],

         c1 = [m1[0] + tx, m1[1] + ty],
         c2 = [m2[0] + tx, m2[1] + ty];

     return {c1: c1, c2: c2, l1: Math.floor(l1), l2: Math.floor(l2)};
   }

   function plotBezierQuad(s1, s2, s3, s4, step) {
     var step = step || 5,

         S1 = findControlPoints(s1, s2, s3),
         S2 = findControlPoints(s2, s3, s4),

         ctr = Math.floor(( (S1.l1 || S1.l2) + (S2.l2 || S2.l1) ) / step),

         p1 = s2,
         p2 = S1.c2,
         p3 = S2.c1,
         p4 = s3,

         // Now do actual bezier math

         dx1 = p2[0] - p1[0], dy1 = p2[1] - p1[1],
         dx2 = p3[0] - p2[0], dy2 = p3[1] - p2[1],
         dx3 = p4[0] - p3[0], dy3 = p4[1] - p3[1],

         ss  = 1.0 / (ctr + 1), ss2 = ss*ss, ss3 = ss2*ss,

         pre1 = 3.0 * ss,
         pre2 = 3.0 * ss2,
         pre4 = 6.0 * ss2,
         pre5 = 6.0 * ss3,

         tmp1x = p1[0] - p2[0] * 2.0 + p3[0],
         tmp1y = p1[1] - p2[1] * 2.0 + p3[1],

         tmp2x = (p2[0] - p3[0])*3.0 - p1[0] + p4[0],
         tmp2y = (p2[1] - p3[1])*3.0 - p1[1] + p4[1],

         pf = p1,

         dfx = (p2[0] - p1[0])*pre1 + tmp1x*pre2 + tmp2x*ss3,
         dfy = (p2[1] - p1[1])*pre1 + tmp1y*pre2 + tmp2y*ss3,

         ddfx = tmp1x*pre4 + tmp2x*pre5,
         ddfy = tmp1y*pre4 + tmp2y*pre5,

         dddfx = tmp2x*pre5,
         dddfy = tmp2y*pre5,

         out = [];

    while (ctr--) {
      pf = [pf[0] + dfx, pf[1] + dfy];

      dfx  += ddfx;
      dfy  += ddfy;
      ddfx += dddfx;
      ddfy += dddfy;

      out.push(pf);
    }

    out.push([p4[0], p4[1]]);

    return out;
  }

  return VectorMap;

}));
